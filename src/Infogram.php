<?php

namespace Drupal\infogram;

/**
 * Infogram helper service.
 */
class Infogram {

  /**
   * The template path used for rendering.
   *
   * @var string
   */
  private $templatePath;

  /**
   * Constructs the Infogram service class.
   */
  public function __construct() {
    $this->templatePath = drupal_get_path('module', 'infogram') . '/templates/infogram.html.twig';
  }

  /**
   * Render the inforgram embed code.
   *
   * @return string
   *   Infogram embed code.
   */
  public function getEmbedCode($id, $format = 'interactive') {
    $markup = \Drupal::service('twig')->loadTemplate($this->templatePath, [
      'data' => [
        'id' => $id,
        'format' => $format,
      ],
    ]);
    return (string) $markup;
  }

}
