<?php

namespace Drupal\infogram\Plugin\Filter;

use Drupal\filter\Plugin\FilterBase;
use Drupal\filter\FilterProcessResult;

/**
 * Infogram WYSIWYG filter.
 *
 * @Filter(
 *   id = "filter_infogram",
 *   title = @Translation("Infogram embed codes"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class FilterInfogram extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {

    $infogramParams = [];
    preg_match('/\[infogram id="([^"]*)" .* format="([^"]*)"/', $text, $infogramParams);

    if (count($infogramParams)) {
      $id = '';
      if (isset($infogramParams[1])) {
        $id = $infogramParams[1];
      }
      $format = $infogramParams[2] ?? 'interactive';
      $text = preg_replace('/\[infogram [^\]]*\]/', \Drupal::service('infogram')->getEmbedCode($id, $format), $text);
    }

    $result = new FilterProcessResult($text);

    return $result;
  }

}
