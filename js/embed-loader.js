/**
 * @file
 * Provides JavaScript for Infogram embed loader.
 */

(function ($) {

  /**
   * Adds the infogram embed loader script.
   */
  Drupal.behaviors.infogramEmbedLoader = {
    attach: function (context) {
      !function (e,t,s,i) {var n = "InfogramEmbeds",o = e.getElementsByTagName("script"),d = o[0],r = /^http:/.test(e.location) ? "http:" : "https:";if(/^\/{2}/.test(i)&&(i = r + i),window[n]&&window[n].initialized) { window[n].process&&window[n].process();} else if(!e.getElementById(s)){var a = e.createElement("script");a.async = 1,a.id = s,a.src = i,d.parentNode.insertBefore(a,d)}}(document,0,"infogram-async","//e.infogram.com/js/dist/embed-loader-min.js");
    }
  }
})(jQuery);
