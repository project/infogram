# Welcome to StackEdit!

Integration of [https://infogram.com](https://infogram.com) graphs embed codes with a wysiwyg filter.

 1. Enable the **Infogram embed codes** filter in your text format configuration (/admin/config/content/formats/manage/{format}).
 2. Use a Wordpress code from your Infogram account:
 ` [infogram id="{id}" format="{format}" prefix="ignored" title="ignored"])`
 3. Enjoy !

## Roadmap
- Resizing in wysiwyg
- Infogram media entities
- Library explorer with API keys

Inspired by the Infogram [wordpress plugin](https://fr.wordpress.org/plugins/infogram/).