<?php

/**
 * @file
 * Infogram module hooks.
 */

/**
 * Implements hook_media_source_info_alter().
 */
function infogram_media_source_info_alter(&$sources) {
  $sources['infogram'] = [
    'id' => 'infogram',
    'label' => t('Infogram'),
    'description' => t('Use https://infogram.com/* urls'),
    'allowed_field_types' => ['string'],
    'default_name_metadata_attribute' => 'infogram',
    'default_thumbnail_filename' => 'infogram-no-thumbnail.png',
    'thumbnail_uri_metadata_attribute' => 'thumbnail_uri',
    'providers' => ['Infogram'],
    'class' => '\Drupal\media\Plugin\media\Source\OEmbed',
    'forms' => ['media_library_add' => '\Drupal\media_library\Form\OEmbedForm'],
  ];
}

/**
 * Implements hook_oembed_resource_data_alter().
 */
function infogram_oembed_resource_data_alter(array &$data, $url) {
  if (strpos($url, 'infogram.com/oembed') !== FALSE) {
    $client = \Drupal::httpClient();
    $thumbnailInfos = pathinfo($data['thumbnail_url']);

    // When to allow_url_fopen = 0, external files infos cannot be retrieved.
    // Creates a temporary file to allow the getimagesize to function on any php.ini settings.
    $tmpInfogramThumbnailPath = Drupal::root() . "/sites/default/files/{$thumbnailInfos['filename']}.{$thumbnailInfos['extension']}";
    $client->request('GET', $data['thumbnail_url'], ['sink' => '/' . $tmpInfogramThumbnailPath]);
    list($thumbnail_width, $thumbnail_height) = getimagesize($tmpInfogramThumbnailPath);
    unlink($tmpInfogramThumbnailPath);

    // Set thumbnail dimensions (missing from infogram oembed provider).
    $data['thumbnail_width'] = $thumbnail_width;
    $data['thumbnail_height'] = $thumbnail_height;
  }
}
